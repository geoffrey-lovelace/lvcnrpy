# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import h5py as h5
from . import errors as err
import numpy as np
import re
from scipy.interpolate import insert, splrep, PPoly
from scipy.interpolate import InterpolatedUnivariateSpline as IUS


class Spec(object):
    """LVCNR Wavform Repository format specification field

    Parameters
    ----------
    name: str
        Lisp-case name of the format specification field `Spec` represents.
    dtype: object
        Type of the format specification field `Spec` represents.
    values: (list of `dtype`), optional
        List of values that the format specification field `Spec` represents
        may take.
    optional: bool
        Specify if the specifcation field is optional.

    Attributes
    ----------
    name: str
        Lisp-case name of the format specification field `Spec` represents.
    dtype: object
        Type of the format specification field `Spec` represents.
    values: (list of `dtype`) or None
        List of values that the format specification field `Spec` represents
        may take. If not specified it will be None.
    """
    name = ""
    dtype = object
    values = None
    optional = False

    def __init(self, name, dtype, values=None):
        self.name = name
        self.dtype = dtype
        self.values = values

    def valid(self, sim):
        """Validate represented field against format specification.

        This will validate that the type of the represented field agrees with
        the format specifictaion and if `self.values` is not `None` that it
        takes one of these values.

        Parameters
        ----------
        sim: lvcnrpy.Sim.Sim
            LVCNR Waveform HDF5 Sim object in which to validate the represented
            format specification field.

        Returns
        -------
        valid: int
            0 if field in `sim` is valid. 1 if the value of the field is not
            in `self.values`. 2 if the value is not of type `self.dtype`. 3 if
            the field is missing.
        """
        value = sim.att(self.name)
        if value is not None:
            if isinstance(value, self.dtype):
                if self.values is None or value in self.values:
                    return err.Valid(self)
                else:
                    return err.InvalidValue(self)
            else:
                return err.WrongType(self)
        else:
            return err.Missing(self)

    def createField(self, sim):
        """Create valid field in simulation of field represented by spec

        Parameters
        ----------
        sim: lvcnrpy.Sim.Sim
            LVCNR Waveform HDF5 Sim object in which to validate the represented
            format specification field.
        """
        if self.dtype == int:
            val = self.values[0] if self.values is not None else 1
        elif self.dtype == float:
            val = self.values[0] if self.values is not None else 1.0
        elif self.dtype == basestring:
            val = self.values[0] if self.values is not None else "1.0"
        sim.attrs[self.name] = val


class GroupSpec(Spec):
    """Specification for `h5.Group` fields"""
    dtype = h5.Group

    def valid(self, sim):
        try:
            value = sim[self.name]
        except:
            value = None
        if value is not None:
            if isinstance(value, self.dtype):
                return err.Valid(self)
            else:
                return err.WrongType(self)
        else:
            return err.Missing(self)

    def createField(self, sim):
        sim.create_group(self.name)


class ROMSplineSpec(Spec):
    """Specification for ROMSpline group fields

    Attributes
    ----------
    subfields: list of str
        List of subfields a ROMSpline field must contain. A standard ROMSpline
        field should contain `(X, Y, deg, errors, tol)` subfields.
    """
    dtype = h5.Group
    subfields = ['X', 'Y', 'deg', 'errors', 'tol']

    def valid(self, sim):
        """Validate represented field against format specification.

        This will validate that the type of the represented field agrees with
        the format specifictaion and that the names of the subfields in the
        group agrees with `self.subfields`.
        """
        try:
            value = sim[self.name]
        except:
            value = None

        if value is not None:
            if isinstance(value, self.dtype):
                s1 = set(self.subfields)
                s2 = set(value.keys())
                diff = s1.symmetric_difference(s2)
                if len(diff) == 0:
                    return err.Valid(self)
                else:
                    return err.InvalidSubFields(self, sim)
            else:
                return err.WrongType(self)
        else:
            return err.Missing(self)

    def createField(self, sim):
        group = sim.create_group(self.name)
        for sub in self.subfields:
            data = np.array([float(i) for i in range(10)])
            group.create_dataset(sub, data=data)


class InterfieldSpec(Spec):
    """Specification for relationship between mutliple fields"""
    dtype = basestring
    validMsg = "(Relationship betwen fields is valid)"
    invalidMsg = "(Relationship betwen fields is invalid)"

    def valid(self, sim):
        return err.ValidInterfield(self)

    def createField(self, sim):
        return


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """A Function for testing approximate equality

    For backward compitibilty when not running with Python >= 3.5.

    **References**

    - https://www.python.org/dev/peps/pep-0485/
    - https://docs.python.org/3.5/library/math.html#math.isclose

    Parameters
    ----------
    a, b: float
        Values to comapre.
    rel_tol: float
        The relative tolerance - it is the maximum allowed difference between a
        and b, relative to the larger absolute value of a or b. For example, to
        set a tolerance of 5%, pass rel_tol=0.05. The default tolerance is
        1e-09, which assures that the two values are the same within about 9
        decimal digits. rel_tol must be greater than zero.
    abs_tol: float
        The minimum absolute tolerance - useful for comparisons near zero.
        abs_tol must be at least zero.

    Returns
    -------
    isclose: bool
        Return True if the values a and b are close to each other and False
        otherwise.
    """
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def ismonotonic(x):
    """A Function for testing is a list/array is monotonic

    **References**

    - https://stackoverflow.com/questions/4983258/python-how-to-check-list-monotonicity

    Parameters
    ----------
    a: list or numpy array of values to test.

    Returns
    -------
    isclose: bool
        Return True if the list x is monotonic and False otherwise.
    """
    dx = np.diff(x)
    return np.all(dx <= 0) or np.all(dx >= 0)


def get_waveform_peak(sim):
    """Determine the peak of an NR waveform

    Determine the maximum point and maximum value of the specified waveform as
    defined by (4) in https://arxiv.org/abs/1703.01076.

    The peak is determined analytically as in general the maximum point will
    not be an element of the `X` dataset of any of the amplitude groups.

    To analytically determine the waveform peak, the amplitude splines are
    constructed from their respective groups. Knots are inserted such that
    all splines share common knots. The splines are then expressed as
    peicewise polynomials which can then be summed to form a single piecewise
    polynomial.

    Parameters
    ----------
    sim: h5.File
        An open HDF5 file like interface that contains LVCNR waveform data.

    Returns
    -------
    tPeak: float
        Maximum point of the specified waveform.
    hPeak:
        Maximum value of the specified waveform.
    """
    # Determine internal knots for waveform piecewise polynomial
    ampKeys = [k for k in sim.keys() if k.startswith('amp_')]
    interiorKnots = np.array([])
    for k in ampKeys:
        interiorKnots = np.union1d(interiorKnots, sim[k]['X'][3:-3])

    # Prepare container for piecewise polynomial
    amp22 = sim['amp_l2_m2']
    tck22 = splrep(amp22['X'][:], amp22['Y'][:]**2, s=0, k=5)
    interiorKnots22 = np.setdiff1d(
        interiorKnots, amp22['X'][3:-3])
    tck22 = reduce(
        lambda tck, knot: insert(knot, tck),
        interiorKnots22,
        tck22)
    pAll = PPoly.from_spline(tck22)

    # Add remaining multipole moments to piecewise polynomial
    ampKeys = [k for k in ampKeys if k != 'amp_l2_m2']
    for k in ampKeys:
        amplm = sim[k]
        tcklm = splrep(amplm['X'][:], amplm['Y'][:]**2, s=0, k=5)
        interiorKnotslm = np.setdiff1d(
            interiorKnots, amplm['X'][3:-3])
        tcklm = reduce(
            lambda tck, knot: insert(knot, tck),
            interiorKnotslm,
            tcklm)
        plm = PPoly.from_spline(tcklm)
        pAll.c += plm.c

    # Find the global maximum point of the piecewise polynomial in domain
    dpAll = pAll.derivative()
    extremumPoints = dpAll.roots()
    t0, t1 = sim['amp_l2_m2/X'][:][np.r_[0, -1]]
    mask = (extremumPoints >= t0) * (extremumPoints <= t1)
    extremumPoints = extremumPoints[mask]
    extremum = pAll(extremumPoints)
    idx = np.argmax(extremum)
    maximumPoint = extremumPoints[idx]
    maximum = extremum[idx]

    return (maximumPoint, maximum)


# General Fields
class Type(Spec):
    """Specification for the `type` field"""
    name = 'type'
    dtype = basestring
    values = ["NRinjection"]


class Format(Spec):
    """Specification for the `Format` field"""
    name = 'Format'
    dtype = int
    values = [1, 2, 3]


class Name(Spec):
    """Specification for the `name` field"""
    name = 'name'
    dtype = basestring


class AlternativeNames(Spec):
    """Specification for the `alternative-names` field"""
    name = 'alternative-names'
    dtype = basestring


class NRGroup(Spec):
    """Specification for the `NR-group` field"""
    name = 'NR-group'
    dtype = basestring


class NRCode(Spec):
    """Specification for the `NR-code` field"""
    name = 'NR-code'
    dtype = basestring


class ModificationDate(Spec):
    """Specification for the `modification-date` field"""
    name = 'modification-date'
    dtype = basestring


class PointOfContactEmail(Spec):
    """Specification for the `point-of-contact-email` field"""
    name = 'point-of-contact-email'
    dtype = basestring


class INSPIREBibtexKeys(Spec):
    """Specification for the `INSPIRE-bibtex-keys` field"""
    name = 'INSPIRE-bibtex-keys'
    dtype = basestring


class License(Spec):
    """Specification for the `license` field"""
    name = 'license'
    dtype = basestring
    values = ['LVC-internal', 'public']


class Lmax(Spec):
    """Specification for the `Lmax` field"""
    name = 'Lmax'
    dtype = int


class SimulationType(Spec):
    """Specification for the `simulation-type` field"""
    name = 'simulation-type'
    dtype = basestring
    values = ['aligned-spins', 'non-spinning', 'precessing']


class AuxiliaryInfo(GroupSpec):
    """Specification for the `auxiliary-info` field"""
    name = 'auxiliary-info'
    optional = True


class NRTechniques(Spec):
    """Specification for the `NR-techniques` field"""
    name = 'NR-techniques'
    dtype = basestring


# Error Assessment
class FilesInErrorSeries(Spec):
    """Specification for the `files-in-error-series` field"""
    name = 'files-in-error-series'
    dtype = basestring


class ComparableSimulation(Spec):
    """Specification for the `comparable-simulation` field"""
    name = 'comparable-simulation'
    dtype = basestring


class ProductionRun(Spec):
    """Specification for the `production-run` field"""
    name = 'production-run'
    dtype = int
    values = [0, 1]


# CBC Parameters
class Object1(Spec):
    """Specification for the `object1` field"""
    name = "object1"
    dtype = basestring
    values = ['BH', 'NS']


class Object2(Spec):
    """Specification for the `object2` field"""
    name = "object2"
    dtype = basestring
    values = ['BH', 'NS']


class Mass1(Spec):
    """Specification for the `mass1` field"""
    name = "mass1"
    dtype = float

    def createField(self, sim):
        sim.attrs[self.name] = 4.0


class Mass2(Spec):
    """Specification for the `mass2` field"""
    name = "mass2"
    dtype = float

    def createField(self, sim):
        sim.attrs[self.name] = 1.0


class Eta(Spec):
    """Specification for the `eta` field"""
    name = "eta"
    dtype = float

    def createField(self, sim):
        sim.attrs[self.name] = 0.16


class FLowerAt1MSUN(Spec):
    """Specification for the `f_lower_at_1MSUN` field"""
    name = "f_lower_at_1MSUN"
    dtype = float


class Spin1x(Spec):
    """Specification for the `spin1x` field"""
    name = "spin1x"
    dtype = float


class Spin1y(Spec):
    """Specification for the `spin1y` field"""
    name = "spin1y"
    dtype = float


class Spin1z(Spec):
    """Specification for the `spin1z` field"""
    name = "spin1z"
    dtype = float


class Spin2x(Spec):
    """Specification for the `spin2x` field"""
    name = "spin2x"
    dtype = float


class Spin2y(Spec):
    """Specification for the `spin2y` field"""
    name = "spin2y"
    dtype = float


class Spin2z(Spec):
    """Specification for the `spin2z` field"""
    name = "spin2z"
    dtype = float


class LNhatx(Spec):
    """Specification for the `LNhatx` field"""
    name = "LNhatx"
    dtype = float


class LNhaty(Spec):
    """Specification for the `LNhaty` field"""
    name = "LNhaty"
    dtype = float


class LNhatz(Spec):
    """Specification for the `LNhatz` field"""
    name = "LNhatz"
    dtype = float


class Nhatx(Spec):
    """Specification for the `Nhatx` field"""
    name = "nhatx"
    dtype = float


class Nhaty(Spec):
    """Specification for the `Nhaty` field"""
    name = "nhaty"
    dtype = float


class Nhatz(Spec):
    """Specification for the `Nhatz` field"""
    name = "nhatz"
    dtype = float


class Omega(Spec):
    """Specification for the `Omega` field"""
    name = "Omega"
    dtype = float


class Eccentricity(Spec):
    """Specification for the `eccentricity` field"""
    name = "eccentricity"
    dtype = float


class MeanAnomaly(Spec):
    """Specification for the `mean_anomaly` field"""
    name = "mean_anomaly"
    dtype = float


class MassOrdering(InterfieldSpec):
    """Specification for the ordering of fields `mass1` and `mass2`"""
    name = "mass-ordering"
    validMsg = "mass1 >= mass2"
    invalidMsg = "mass1 < mass2"

    def valid(self, sim):
        mass1Valid = Mass1().valid(sim)
        mass2Valid = Mass2().valid(sim)

        if not isinstance(mass1Valid, err.Valid):
            return err.InvalidInterfields(self)
        if not isinstance(mass2Valid, err.Valid):
            return err.InvalidInterfields(self)

        mass1 = sim.att('mass1')
        mass2 = sim.att('mass2')
        if mass1 >= mass2 or isclose(mass1, mass2, rel_tol=1e-5):
            return err.ValidInterfield(self)
        else:
            return err.InvalidInterfield(self)


# Format 2
class Mass1VsTime(ROMSplineSpec):
    """Specification for the `mass1-vs-time` field"""
    name = 'mass1-vs-time'

    def createField(self, sim):
        super(Mass1VsTime, self).createField(sim)
        sim['mass1-vs-time/Y'][:] = np.array([4.0]*10)


class Mass2VsTime(ROMSplineSpec):
    """Specification for the `mass2-vs-time` field"""
    name = 'mass2-vs-time'

    def createField(self, sim):
        super(Mass2VsTime, self).createField(sim)
        sim['mass2-vs-time/Y'][:] = np.array([1.0]*10)


class Spin1xVsTime(ROMSplineSpec):
    """Specification for the `spin1x-vs-time` field"""
    name = 'spin1x-vs-time'


class Spin1yVsTime(ROMSplineSpec):
    """Specification for the `spin1y-vs-time` field"""
    name = 'spin1y-vs-time'


class Spin1zVsTime(ROMSplineSpec):
    """Specification for the `spin1z-vs-time` field"""
    name = 'spin1z-vs-time'


class Spin2xVsTime(ROMSplineSpec):
    """Specification for the `spin2x-vs-time` field"""
    name = 'spin2x-vs-time'


class Spin2yVsTime(ROMSplineSpec):
    """Specification for the `spin2y-vs-time` field"""
    name = 'spin2y-vs-time'


class Spin2zVsTime(ROMSplineSpec):
    """Specification for the `spin2z-vs-time` field"""
    name = 'spin2z-vs-time'


class Position1xVsTime(ROMSplineSpec):
    """Specification for the `position1x-vs-time` field"""
    name = 'position1x-vs-time'


class Position1yVsTime(ROMSplineSpec):
    """Specification for the `position1y-vs-time` field"""
    name = 'position1y-vs-time'


class Position1zVsTime(ROMSplineSpec):
    """Specification for the `position1z-vs-time` field"""
    name = 'position1z-vs-time'


class Position2xVsTime(ROMSplineSpec):
    """Specification for the `position2x-vs-time` field"""
    name = 'position2x-vs-time'


class Position2yVsTime(ROMSplineSpec):
    """Specification for the `position2y-vs-time` field"""
    name = 'position2y-vs-time'


class Position2zVsTime(ROMSplineSpec):
    """Specification for the `position2z-vs-time` field"""
    name = 'position2z-vs-time'


class LNhatxVsTime(ROMSplineSpec):
    """Specification for the `LNhatx-vs-time` field"""
    name = 'LNhatx-vs-time'


class LNhatyVsTime(ROMSplineSpec):
    """Specification for the `LNhaty-vs-time` field"""
    name = 'LNhaty-vs-time'


class LNhatzVsTime(ROMSplineSpec):
    """Specification for the `LNhatz-vs-time` field"""
    name = 'LNhatz-vs-time'


class OmegaVsTime(ROMSplineSpec):
    """Specification for the `Omega-vs-time` field"""
    name = 'Omega-vs-time'
    invalidMsg = "Omega is not monotonic: Suggest downgrading to Format=1"

    def valid(self, sim):
        romSplineValid = super(OmegaVsTime, self).valid(sim)
        if not isinstance(romSplineValid, err.Valid):
            return romSplineValid

        Omegas = sim['Omega-vs-time/Y'][:]
        if ismonotonic(Omegas):
            return romSplineValid
        else:
            return err.InvalidSequence(self)


class MassVsTimeOrdering(InterfieldSpec):
    """Specification for the ordering of fields `mass1/mass2-vs-time``"""
    name = "mass-vs-time-ordering"
    validMsg = "mass1-vs-time >= mass2-vs-time"
    invalidMsg = "mass1-vs-time < mass2-vs-time"

    def valid(self, sim):
        mass1VsTimeValid = Mass1VsTime().valid(sim)
        mass2VsTimeValid = Mass2VsTime().valid(sim)

        if not isinstance(mass1VsTimeValid, err.Valid):
            return err.InvalidInterfields(self)
        if not isinstance(mass2VsTimeValid, err.Valid):
            return err.InvalidInterfields(self)

        mass1 = sim['mass1-vs-time/Y'][0]
        mass2 = sim['mass2-vs-time/Y'][0]
        if mass1 >= mass2 or isclose(mass1, mass2, rel_tol=1e-5):
            return err.ValidInterfield(self)
        else:
            return err.InvalidInterfield(self)


# Format 3
class RemnantMassVsTime(ROMSplineSpec):
    """Specification for the `remnant-mass-vs-time`` field"""
    name = 'remnant-mass-vs-time'


class RemnantSpinxVsTime(ROMSplineSpec):
    """Specification for the `remnant-spinx-vs-time` field"""
    name = 'remnant-spinx-vs-time'


class RemnantSpinyVsTime(ROMSplineSpec):
    """Specification for the `remnant-spiny-vs-time` field"""
    name = 'remnant-spiny-vs-time'


class RemnantSpinzVsTime(ROMSplineSpec):
    """Specification for the `remnant-spinz-vs-time` field"""
    name = 'remnant-spinz-vs-time'


class RemnantPositionxVsTime(ROMSplineSpec):
    """Specification for the `remnant-positionx-vs-time` field"""
    name = 'remnant-positionx-vs-time'


class RemnantPositionyVsTime(ROMSplineSpec):
    """Specification for the `remnant-positiony-vs-time` field"""
    name = 'remnant-positiony-vs-time'


class RemnantPositionzVsTime(ROMSplineSpec):
    """Specification for the `remnant-positionz-vs-time` field"""
    name = 'remnant-positionz-vs-time'


# Strain modes
class Phaselm(ROMSplineSpec):
    """Specification for all `phase_lX_mY` fields

    Parameters
    ----------
    l, m: int, optional
        Degree and order of the phase mode field to represent. Default is the
        (2,2) phase mode.
    name: str, optionl
        Name of phase field in simulation. This must be of the form
        `phase_l<int>_m<int>`. If provided, parameters `l` and `m` are ignored.

    Attributes
    ----------
    l, m: int, optional
        Degree and order of the phase mode field to represent. Default is the
        (2,2) phase mode.
    """
    name = "phase_l2_m2"
    l = 2
    m = 2

    def __init__(self, l=2, m=2, name=None):
        if name is None:
            name = "phase_l{0:d}_m{1:d}".format(l, m)
        else:
            l, m = re.split(r'phase_l|_m', name)[1:]
        self.l = int(l)
        self.m = int(m)
        self.name = name

    def valid(self, sim):
        if abs(self.m) > self.l:
            return err.InvalidModeName(self)
        else:
            return super(Phaselm, self).valid(sim)

    @classmethod
    def getlm(self, sim):
        """Get a list of phase modes available in an LVC NR simulation

        This method returns all fields representing modes that have names that
        match the syntax required by `self.name`. This are only candidate mode
        fields and may not be valid.

        Parameters
        ----------
        sim: lvcnrpy.Sim.Sim
            LVCNR Waveform HDF5 Sim object in which to validate the represented
            format specification field.

        Returns
        -------
        lm: `lvcnr.format.specs.Phaselm`
            Returns a list of candidate phase modes in `sim`.
        """
        lmTest = r'^phase_l[0-9]{1,2}_m-?[0-9]{1,2}$'
        keys = [k for k in sim.keys() if re.match(lmTest, k) is not None]
        lm = [Phaselm(name=k) for k in keys]

        return lm


class Amplm(ROMSplineSpec):
    """Specification for all `amp_lX_mY` fields

    Parameters
    ----------
    l, m: int, optional
        Degree and order of the amp mode field to represent. Default is the
        (2,2) phase mode.
    name: str, optionl
        Name of amp field in simulation. This must be of the form
        `amp_l<int>_m<int>`. If provided, parameters `l` and `m` are
        ignored.

    Attributes
    ----------
    l, m: int, optional
        Degree and order of the amp mode field to represent. Default is the
        (2,2) phase mode.
    """
    name = "amp_l2_m2"
    l = 2
    m = 2

    def __init__(self, l=2, m=2, name=None):
        if name is None:
            name = "amp_l{0:d}_m{1:d}".format(l, m)
        else:
            l, m = re.split(r'amp_l|_m', name)[1:]
        self.l = int(l)
        self.m = int(m)
        self.name = name

    def valid(self, sim):
        if abs(self.m) > self.l:
            return err.InvalidModeName(self)
        else:
            return super(Amplm, self).valid(sim)

    @classmethod
    def getlm(self, sim):
        """Get a list of amp modes available in an LVC NR simulation

        This method returns all fields representing modes that have names that
        match the syntax required by `self.name`. This are only candidate mode
        fields and may not be valid.

        Parameters
        ----------
        sim: lvcnrpy.Sim.Sim
            LVCNR Waveform HDF5 Sim object in which to validate the represented
            format specification field.

        Returns
        -------
        lm: list of `lvcnr.format.specs.Amplm`
            Returns a list of candidate amp modes in `sim`.
        """
        lmTest = r'^amp_l[0-9]{1,2}_m-?[0-9]{1,2}$'
        keys = [k for k in sim.keys() if re.match(lmTest, k) is not None]
        lm = [Amplm(name=k) for k in keys]

        return lm


class PeakNearZero(InterfieldSpec):
    """Specification for the location of the waveform peak"""
    name = "peak-near-zero"
    validMsg = "waveform peak is near zero"
    invalidMsg = "waveform peak is not near zero"

    def valid(self, sim, tol=10.0):
        # Validate multipole moment amplitudes
        ampModes = Amplm.getlm(sim)
        for ampMode in ampModes:
            ampModeValid = ampMode.valid(sim)
            if not isinstance(ampModeValid, err.Valid):
                self.invalidMsg = "amp_l{0:d}_m{1:d} is invalid".format(
                    ampMode.l, ampMode.m)
                return err.InvalidInterfields(self)

        maximumPoint, maximum = get_waveform_peak(sim)

        if np.abs(maximumPoint) < tol:
            self.validMsg = (
                "waveform peak is at {0:.2f}M"
                " which is less than {1:.2f}M from zero").format(
                    maximumPoint, tol)
            return err.ValidInterfield(self)
        else:
            self.invalidMsg = (
                "waveform peak is at {0:.2f}M"
                " which is greater than {1:.2f}M from zero").format(
                    maximumPoint, tol)
            return err.InvalidInterfield(self)


class PhaseSense(InterfieldSpec):
    """Specification for the sense of the waveform pahse"""
    name = "phase-sense"
    validMsg = "(2,2) phase is decreasing on average"
    invalidMsg = "(2,2) phase is not decreasing on average"

    def valid(self, sim):
        # Validate (2,2) multipole moment phase
        phaseMode = Phaselm(name='phase_l2_m2')
        phaseModeValid = phaseMode.valid(sim)
        if not isinstance(phaseModeValid, err.Valid):
            self.invalidMsg = "phase_l2_m2 is invalid"
            return err.InvalidInterfields(self)

        # Determine sense from middle of pre-merger waveform
        phase = np.array([sim['phase_l2_m2/X'][:], sim['phase_l2_m2/Y'][:]])
        t0 = phase[0][0]*(3.0/4.0)
        if t0 >= 0:
            self.invalidMsg = "no (2,2) phase before 0M"
            return err.InvalidInterfields(self)
        t1 = t0*(1.0/3.0)
        X = np.arange(t0, t1, 0.1)
        Y = IUS(*phase, k=5)(X)
        sense = np.mean(np.diff(Y))

        if sense < 0:
            return err.ValidInterfield(self)
        else:
            return err.InvalidInterfield(self)
