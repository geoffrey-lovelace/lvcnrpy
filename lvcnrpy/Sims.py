# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from h5py import File
import os
import glob
from .Sim import Sim
from . import LVCNR_DATADIR
from operator import concat
import json


class Sims:
    """Collection of simulations from the LVC-NR Waveform Repository

    Instancing this obejct without the option `paths` parameter will provide
    access to all hdf5 NR files within the LVC-NR Waveform Repository.

    Parameters
    ----------
    paths: list of str, optional
        Optional list of paths to hdf5 NR files.
    """

    def __init__(self, paths=None):
        if paths is None:
            self.paths = self.getAllPaths()
        elif all(isinstance(s, basestring) for s in paths):
            self.paths = paths
        else:
            self.paths = []

    def getAllPaths(self):
        """Find all paths of hdf5 NR files in the LVC-NR Waveform Repository

        Returns
        -------
        simualtions: list of str
            All hdf5 NR file paths
        """
        # Find all valid (no `.` prefix) subdirectories of LVCNR_DATADIR
        children = glob.glob('{0:s}/*/'.format(LVCNR_DATADIR))
        descendants = [[[y[0], y[2]] for y in os.walk(x)] for x in children]
        descendants = reduce(concat, descendants)

        # Find all hdf5 files
        paths = [[os.path.join(d[0], f) for f in d[1]] for d in descendants]
        paths = reduce(concat, paths)
        simPaths = [path for path in paths if path.endswith('.h5')]

        # Remove RAW Cardiff data (temporary hack)
        simPaths = [p for p in simPaths if 'Psi4ModeDecomp' not in p]

        # Instance Sim object for each simulation
        # sims = [Sim(simPath, 'r') for simPath in simPaths]

        return simPaths  # sims

    def __len__(self):
        """List like length of contained simulations

        Returns
        -------
        length: int
            Length of `self.paths`
        """
        return len(self.paths)

    def __getitem__(self, index):
        """List like access to the contained simulations

        Parameters
        ----------
        index: int
            Index of simulation to return.

        Returns
        -------
        simualtion: lvcnrpy.Sim.Sim
            The requeted simulation object.
        """
        if isinstance(index, slice):
            return Sims(self.paths[index])
        else:
            return Sim(self.paths[index], 'r')

    def getJSON(self):
        """Get attributes of the level 1 format specification for simulations

        Returns
        -------
        simulations: str
            This will return a JSON representation of all the attributes of the
            level 1 format specification for all the simulations represented by
            this object.
        """
        return json.dumps([sim.getAttrs() for sim in self], indent=2)
