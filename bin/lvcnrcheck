#!/usr/bin/env python
#
# Copyright (C) 2016 Edward Fauchon-Jones
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import numpy as np
from lvcnrpy.Sim import Sim
from colorama import Fore, Back, Style
import h5py as h5
from lvcnrpy.format.format import format1, format1Interfield
from lvcnrpy.format.format import format2, format2Interfield
from lvcnrpy.format.format import format3
import lvcnrpy.format.specs as specs
import lvcnrpy.format.errors as err
import sys

# Command line setup + parsing
parser = argparse.ArgumentParser(
    description="Check hdf5 NR data meet LVCNR format specifications",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('file', help='name of file to check')
parser.add_argument(
    '-c', '--col',
    help='use coloured output', action='store_true')
parser.add_argument(
    '-f', '--format',
    help=(
        "format level to use, if not provided the format level specified in "
        "file will be used, if the format field is missing from file format "
        "level 1 will be used"),
    type=int, choices=[1, 2, 3])
args = parser.parse_args()

# Define output syntax for a single field report
FIELD = u'- [{0:s}{{0:s}}{1:s}] {{1:s}} ({{2:}}) {{3:}}'
INTERFIELD = u'- [{0:s}{{0:s}}{1:s}] {{1:s}} {{2:}}'
if args.col:
    # Define possible field output templates
    FIELD_VALID = FIELD.format(Fore.GREEN, Style.RESET_ALL)
    FIELD_INVALID = FIELD.format(Fore.RED, Style.RESET_ALL)
    INTERFIELD_VALID = INTERFIELD.format(Fore.GREEN, Style.RESET_ALL)
    INTERFIELD_INVALID = INTERFIELD.format(Fore.RED, Style.RESET_ALL)
else:
    # Define possible field output templates without color
    FIELD_VALID = FIELD.format('', '')
    FIELD_INVALID = FIELD.format('', '')
    INTERFIELD_VALID = INTERFIELD.format('', '')
    INTERFIELD_INVALID = INTERFIELD.format('', '')


def checkField(sim, field):
    # Check validity of field against specification class `field`
    valid = field.valid(sim)
    msg = valid.msg if valid.msg is not None else ''

    # Handle case of missing field value
    if isinstance(valid, err.Missing):
        print FIELD_INVALID.format(
            valid.name, field.name, 'undefined', msg).strip('')
        if field.optional:
            return 0
        else:
            return 1

    # Condition on being an attribute
    isAttribute = not isinstance(
        field, (specs.ROMSplineSpec, specs.GroupSpec))

    # If field exists condition on its type of validity
    value = sim.att(field.name) if isAttribute else type(sim[field.name])
    if isinstance(valid, err.Valid):
        print FIELD_VALID.format(valid.name, field.name, value, msg).strip()
        return 0
    else:
        print FIELD_INVALID.format(valid.name, field.name, value, msg).strip()
        return 1


def checkInterfield(sim, interfield):
    # Check validity of interfield against specification class `interfield`
    valid = interfield.valid(sim)

    # Condition on its type of validity
    if isinstance(valid, err.ValidInterfield):
        print INTERFIELD_VALID.format(
            valid.name, interfield.name, valid.msg).strip()
        return 0
    else:
        print INTERFIELD_INVALID.format(
            valid.name, interfield.name, valid.msg).strip()
        return 1


if __name__ == '__main__':

    # Initialise checkInt. checkInt = 0 for pass and >0 for fail.
    checkInt = 0

    sim = Sim(args.file)

    # Format 1
    print('# Format 1\n')

    for groupKey, group in format1.items():
        print("## {0:s}\n".format(groupKey))

        for fieldKey, field in group.items():
            checkInt += checkField(sim, field())

        print('')

    # Format 1 interfield checks
    print('# Format 1 (Interfield)\n')

    for interfieldKey, interfield in format1Interfield.items():
        checkInt += checkInterfield(sim, interfield())

    print('')

    # Determine format level to check
    fmt = args.format or sim.att('Format') or 1

    if fmt > 1:
        # Format 2
        print('# Format 2\n')

        for fieldKey, field in format2.items():
            checkInt += checkField(sim, field())

        print('')

        # Format 2 interfield checks
        print('# Format 2 (Interfield)\n')

        for interfieldKey, interfield in format2Interfield.items():
            checkInt += checkInterfield(sim, interfield())

        print('')

    if fmt > 2:
        # Format 3
        print('# Format 3\n')

        for fieldKey, field in format3.items():
            checkInt += checkField(sim, field())

        print('')

    # Check phase modes
    phaseModes = specs.Phaselm.getlm(sim)
    if len(phaseModes) > 0:
        print('# Phase Modes\n')
    for phaseMode in phaseModes:
        checkInt += checkField(sim, phaseMode)

    print ('')

    # Check amp modes
    ampModes = specs.Amplm.getlm(sim)
    if len(ampModes) > 0:
        print('# Amplitude Modes\n')
    for ampMode in ampModes:
        checkInt += checkField(sim, ampMode)

    # Return exit code based on field checks
    sys.exit(0) if checkInt == 0 else sys.exit(1)
